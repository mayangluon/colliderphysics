///////////////////////////////////////////////////////
//Collider Physics, Sep. 12, 2018                    // 
//Example 1: generate an event using Pythia          //
//Yang Ma @ PITTPACC, 9/9/2018                       //
///////////////////////////////////////////////////////

//pp -->ttbar @ 8TeV  
#include "Pythia8/Pythia.h"                        //Pythia header
using namespace Pythia8;

int main(){
Pythia pythia;

pythia.readString("Top:gg2ttbar = on");            //Switch on process, gg->ttbar
pythia.readString("Top:qqbar2ttbar = on");         //Switch on process, qqbar->ttbar
pythia.readString("Beams:eCM = 8000.");            //8 TeV
pythia.init();                                     //pp beams default

//Generate event(s)
pythia.next();
return 0;
}
