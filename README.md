# colliderphysics
Here are the support materials for the presentation on Pythia 8 and Madgraph on Sep. 12, 2018.
The Pythia 8 and Madgraph packages are included here and one can run "./makepythia" to install Pythia 8 and "./makeMG5" to install Madgraph.
All the sample codes have been tested on Ubuntu and MacOS. If you are using Windows operating system, I would recommend to install "Ubuntu 16" in the "Oracle VM VirtualBox".

Following are the websites where you can get Oracle VM VirtualBox and Ubuntu:
https://www.virtualbox.org/wiki/Downloads
http://releases.ubuntu.com/16.04/ 

P.S. One might need to install "git" and "gfortran" in order by run "sudo apt install git gfortran build-essential" in Ubuntu. 

