///////////////////////////////////////////////////////
//Collider Physics, Sep. 12, 2018                    // 
//Example 3: Histograms                              //
//           PT distribution of ttbar production     //
//Yang Ma @ PITTPACC, 9/9/2018                       //
///////////////////////////////////////////////////////

//pp -->ttbar @ 8TeV
#include "Pythia8/Pythia.h"                         //Pythia header
using namespace Pythia8;

int main(){
Pythia pythia;

pythia.readString("Top:gg2ttbar = on");            //Switch on process, gg->ttbar
pythia.readString("Top:qqbar2ttbar = on");         //Switch on process, qqbar->ttbar
pythia.readString("Beams:eCM = 8000.");            //8 TeV
pythia.init();                                     //pp beams default

Hist PT("top transverse momentum", 100, 0., 200.); //Book the histograms

//Event loop
for (int iEvent = 0; iEvent < 5; ++iEvent){
	pythia.next();

//Particle loop
	int iTop = 0;
	for (int i = 0; i < pythia.event.size(); ++i){  
		//cout << "i=" <<i << ", id=" << pythia.event[i].id()
		     //<< ",    " << pythia.event[i].name()
		     //<< ", px=" << pythia.event[i].px()
		     //<< ", py=" << pythia.event[i].py()
		     //<< ", pz=" << pythia.event[i].pz()
		     //<< ", e=" << pythia.event[i].e()
		     //<< endl;
		if (pythia.event[i].id()==6) iTop= i;
	}
//Particle loop end
//  cout << "iTop=" <<iTop << ", id=" << pythia.event[iTop].id();
	PT.fill ( pythia.event[iTop].pT() );            //Fill the histograms in each event
}
//Event loop end
cout << PT ;                                        //Write out the histograms
PT.table("ttbarPT");                                //Out histogram to a file
pythia.stat();                                      //number of events and estimate cross section
return 0;
}
