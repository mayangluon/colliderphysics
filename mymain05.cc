///////////////////////////////////////////////////////
//Collider Physics, Sep. 12, 2018                    // 
//Example 5: Input ".lhe" files from MG5             //
//Yang Ma @ PITTPACC, 9/9/2018                       //
///////////////////////////////////////////////////////

//pp -->ttbar   
#include "Pythia8/Pythia.h" 
using namespace Pythia8;

int main(){
Pythia pythia;

// Initialize Les Houches Event File run. List initialization information.
pythia.readString("Beams:frameType = 4");          // beam type is imported from .lhe file
pythia.readString("Beams:LHEF = ppttb.lhe");       //read in .lhe file
pythia.readString("PartonLevel:ISR = on"); 
pythia.readString("PartonLevel:FSR = on"); 

pythia.init();                                     //pp beams default

Hist PT("top transverse momentum", 100, 0., 200.); //Book the histograms

//Event loop
for (int iEvent = 0; ; ++iEvent){
	if (!pythia.next()) 
		{
			// If failure because reached end of file then exit event loop.
			if (pythia.info.atEndOfFile()) break; 
		}
//Particle loop
	int iTop = 0;
	for (int i = 0; i < pythia.event.size(); ++i){  
		//cout << "i=" <<i << ", id=" << pythia.event[i].id()
		     //<< ",    " << pythia.event[i].name()
		     //<< ", px=" << pythia.event[i].px()
		     //<< ", py=" << pythia.event[i].py()
		     //<< ", pz=" << pythia.event[i].pz()
		     //<< ", e=" << pythia.event[i].e()
		     //<< endl;
		if (pythia.event[i].id()==6) iTop= i;
	}
//Particle loop end
//  cout << "iTop=" <<iTop << ", id=" << pythia.event[iTop].id();
	PT.fill ( pythia.event[iTop].pT() );            //Fill the histograms in each event
}
//Event loop end
cout << PT ;                                        //Write out the histograms
PT.table("ttbarPT");                                //Out histogram to a file
pythia.stat();                                      //number of events and estimate cross section
return 0;
}
