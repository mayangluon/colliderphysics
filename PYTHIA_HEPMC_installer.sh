#!/bin/bash

TOOLS=""


echo "-- Begin installing HEPMC ---------------------" 
echo " Download HEPMC2 "
wget http://lcgapp.cern.ch/project/simu/HepMC/download/HepMC-2.06.09.tar.gz

tar -xvf HepMC-2.06.09.tar.gz
echo " Unpack HEPMC"
rm HepMC-2.06.09.tar.gz
echo " Create HEPMC directory"
mkdir HEPMC2
cd HepMC-2.06.09/
echo " Configure HEPMC"
./configure --prefix=$TOOLS/HEPMC2 --with-momentum=GEV --with-length=MM
echo " Compile HEPMC"
make
echo " Install HEPMC"
make install
cd ..
echo " Finished HEPMC installation"

echo "-- Begin installing PYTHIA8 ---------------------" 
echo " Download PYTHIA8 " 
wget http://home.thep.lu.se/~torbjorn/pythia8/pythia8243.tgz
echo " Unpack PYTHIA8"
tar -xvf pythia8243.tgz
echo " Remove the package file "
rm pythia8243.tgz
cd pythia8243/
echo " Configure PYTHIA8 " 
./configure --with-hepmc2=$TOOLS/HEPMC2
echo " Compile PYTHIA8"
make
make install
echo " Finished PYTHIA installation"
cd ..

