///////////////////////////////////////////////////////
//Collider Physics, Sep. 12, 2018                    // 
//Example 4: Input “card” files                      //
//Yang Ma @ PITTPACC, 9/9/2018                       //
///////////////////////////////////////////////////////

//pp -->ttbar   
#include "Pythia8/Pythia.h" 
using namespace Pythia8;

int main( int argc, char* argv[]){                  //Note the diffenence here, "(int argc, char* argv[])"           
Pythia pythia;

pythia.readFile(argv[1]);                           //read the .cmnd file
pythia.init();

//Book the histograms, PT, rapidity, etc
Hist PT("top transverse momentum", 100, 0., 400.);
Hist eta("top pseudorapidity", 100, -5, 5.);
Hist y("top truerapidity", 100, -5, 5.);
Hist mass("m(ttbar) [GeV]", 1000, 350., 2000.);

//define nEvent to load the number of events in .cmnd file
//int nEvent = pythia.settings.mode("Main:numberOfEvents"); 
for (int iEvent = 0; iEvent < 10000; ++iEvent){     //Change this 10000 to a smaller number for tests, also can replace number with nEvent 
	pythia.next();

	int iTop = 0;
	int iTopbar = 0;
	for (int i = 0; i < pythia.event.size(); ++i){   
		if (pythia.event[i].id()==6) iTop= i;
		else if (pythia.event[i].id()==-6) iTopbar= i;
	}

	PT.fill ( pythia.event[iTop].pT() );
	eta.fill ( pythia.event[iTop].eta() );
	y.fill ( pythia.event[iTop].y() );
	mass.fill((pythia.event[iTop].p() + pythia.event[iTopbar].p()).mCalc());
}

cout << PT << eta << mass; 
PT.table("pp_t_PT");
eta.table("pp_t_eta");
y.table("pp_t_y");
mass.table("pp_t_m");
pythia.stat();
return 0;
}
