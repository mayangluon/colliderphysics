///////////////////////////////////////////////////////
//Collider Physics, Sep. 12, 2018                    // 
//Practice:  pp-->tth @ 14TeV                        //
//Yang Ma @ PITTPACC, 9/9/2018                       //
///////////////////////////////////////////////////////

#include "Pythia8/Pythia.h"
using namespace Pythia8;

//int main( int argc, char* argv[]){
int main(){
	Pythia pythia;
	pythia.readString("HiggsSM:gg2Httbar = on");
	pythia.readString("HiggsSM:qqbar2Httbar = on");
	pythia.readString("Beams:eCM = 14000.");
	pythia.readString("PartonLevel:ISR = on");
	pythia.readString("PartonLevel:FSR = on");

//	pythia.readFile("pptth.cmnd");   
	pythia.init();

	Hist pTt("top transverse momentum", 250, 0., 1000.);
	Hist pTh("higgs transverse momentum", 250, 0., 1000.);
	Hist mass("m(th) [GeV]", 1000, 295., 2000.);

	for(int iEvent=0;iEvent<10000;++iEvent){
		pythia.next();
		int iTop = 0;
		int iH = 0;
		for (int i=0;i<pythia.event.size(); ++i){
			if (pythia.event[i].id()==6) iTop = i;
			else if (pythia.event[i].id()==25) iH= i;
		} 
		pTt.fill(pythia.event[iTop].pT());
		pTh.fill(pythia.event[iH].pT());
		mass.fill((pythia.event[iH].p() + pythia.event[iTop].p()).mCalc());

	}
	pTt.table("pp_tth_PTt_14");
	pTh.table("pp_tth_PTh_14");
	mass.table("pp_tth_mth_14");

	cout<<pTt<<pTh<<mass;
	pythia.stat();	
	return 0;
}
